#include "World.h"
#include "Server.h"
#include "Character.h"
#include "Chunk.h"

Character* World::addCharacter(const Addr& addr) {
    auto* character = new Character(acc++, &scene, Point{4, 4}, addr);
    clientCharacters[addr] = character;
    return character;
}

void World::remCharacter(const Addr& addr) {
    auto* character = clientCharacters[addr];
    clientCharacters.erase(addr);
    delete character;
}

void World::moveCharacter(const Addr& addr, const Data::ClientMessage& msg) {
    auto* character = clientCharacters[addr];
    auto* data = std::get_if<Data::Movement>(&msg.typeData);
    if (data) character->move(data->point);
}

void World::attackCharacter(const Addr& addr, const Data::ClientMessage& msg) {
    
}

void World::addClient(const Addr& addr) {
    auto* character = addCharacter(addr);
    character->sendChunks();
    character->giveControl();
}

void World::remClient(const Addr& addr) {
    remCharacter(addr);
}

World::World()
: acc(0)
, scene(0, 4, 4)
{
    
}

void World::recvMessage(const Addr& addr, const Data::ClientMessage& msg) {
    if (
        msg.type != Data::ClientMessage::Type::Connect &&
        !clientCharacters.contains(addr)
    ) return;
    
    switch(msg.type) {
    case Data::ClientMessage::Type::Connect:
        if (!clientCharacters.contains(addr))
            addClient(addr);
        break;
    case Data::ClientMessage::Type::Disconnect:
        remClient(addr);
        break;
    case Data::ClientMessage::Type::Move:
        moveCharacter(addr, msg);
        break;
    case Data::ClientMessage::Type::Attack:
        attackCharacter(addr, msg);
        break;
    default:
        break;
    }
}

void World::update(float delta) {
    for (auto& chunk : scene.chunks)
    for (auto* character : chunk.characters)
        character->update(delta);
    
    for (auto& chunk : scene.chunks)
    for (auto* character : chunk.characters) {
        if (!character->dirty) continue;
        Data::ServerMessage msg;
        msg.type = Data::ServerMessage::Type::SyncCharacter;
        msg.typeData = (Data::Character)*character;
        auto chunks = character->getNearbyChunks();
        for (auto* chunk : chunks)
        for (auto* character : chunk->characters)
            server->addMessage(character->client, msg);
        character->dirty = false;
    }
}
