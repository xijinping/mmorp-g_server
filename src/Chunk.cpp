#include "Chunk.h"
#include "Character.h"

Chunk::Chunk(int id, const Point& pos)
: characters()
{
    this->id = id;
    this->pos = pos;
    realPos.x *= Data::Chunk::size;
    realPos.y *= Data::Chunk::size;
    
    for (auto& row : tileMap)
    for (auto& tile : row)
        tile = 0;
}

uint32_t& Chunk::getFlags(const Point& point) {
    auto index = sub(point, realPos);
    return tileMap[index.y][index.x];
}

Data::Chunk Chunk::toData() {
    Data::Chunk ret;
    ret.id = id;
    ret.pos = pos;
    ret.characters.reserve(characters.size());
    for (auto* character : characters)
        ret.characters.push_back((Data::Character)(*character));
    return ret;
}
