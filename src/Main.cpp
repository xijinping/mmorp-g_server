#include "Server.h"
#include "World.h"

Server* server;
World* world;

int main(int argc, char** argv) {
    Server s(5000);
    World w;
    
    server = &s;
    world = &w;
    
    for(;;) {
        server->recvMessages();
        world->update(0.1);
        server->sendMessages();
    }
    
    return 0;
}
