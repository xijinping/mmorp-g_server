#include "Character.h"
#include "Scene.h"
#include "Chunk.h"
#include "Server.h"

static Point toChunkPos(const Point& point) {
    return Point{point.x/Data::Chunk::size, point.y/Data::Chunk::size};
}

Character::Character(int id, Scene* s, Point spawn, Addr owner)
: Data::Character()
, scene(s)
, client(owner)
, dirty(true)
{
    this->id = id;
    type = Data::Character::Type::Player;
    faction = Data::Character::Faction::Friendly;
    
    pos = spawn;
    
    this->scene = scene;
    chunk = scene->getChunk(toChunkPos(pos));
    
    chunk->getFlags(pos) = 1;
    chunk->characters.insert(this);
}

Character::~Character() {
    chunk->getFlags(pos) = 0;
    chunk->characters.erase(this);
}

static bool inMinQuadrant(int pos, int start) {
    static constexpr auto halfChunk = Data::Chunk::size/2;
    return pos%Data::Chunk::size < halfChunk && start-1 >= 0;
}

void Character::move(const Point& point) {
    if (length(sub(pos, point)) > 1) return;
    if (!inRect(point, scene->rect)) return;
    if (chunk->getFlags(point)) return;
    
    auto* newChunk = scene->getChunk(toChunkPos(point));
    chunk->getFlags(pos) = 0;
    newChunk->getFlags(point) = 1;
    
    if (newChunk != chunk) {
        chunk->characters.erase(this);
        newChunk->characters.insert(this);
        chunk = newChunk;
    } else {
        auto start = toChunkPos(pos);
        if (
            (inMinQuadrant(pos.x, start.x) != inMinQuadrant(point.x, start.x))
         || (inMinQuadrant(pos.y, start.y) != inMinQuadrant(point.y, start.y))
        ) sendChunks();
    }
    
    pos = point;
    dirty = true;
}

std::vector<Chunk*> Character::getNearbyChunks() {
    std::vector<Chunk*> ret;
    ret.reserve(4);
    
    auto start = toChunkPos(pos);
    
    if (inMinQuadrant(pos.y, start.y))
        --start.y;
    if (inMinQuadrant(pos.x, start.x))
        --start.x;
    
    for (auto y = start.y; y < start.y+2 && y < scene->height; ++y)
    for (auto x = start.x; x < start.x+2 && x < scene->width; ++x)
        ret.push_back(scene->getChunk(x, y));
    
    return ret;
}

void Character::update(float delta) {
    
}

void Character::sendChunks() {
    Data::ServerMessage msg;
    msg.type = Data::ServerMessage::Type::SyncChunk;
    auto chunks = getNearbyChunks();
    for (auto* chunk : chunks) {
        msg.typeData = chunk->toData();
        server->addMessage(client, msg);
    }
}

void Character::giveControl() {
    Data::ServerMessage msg;
    msg.type = Data::ServerMessage::Type::GiveControl;
    msg.typeData = id;
    server->addMessage(client, msg);  //let the client "control" the character
}
