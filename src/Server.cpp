#include "Server.h"
#include "World.h"
#include "fcntl.h"

Server::Server(short port)
: acc(0)
{
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (!fd) std::exit(1);
    fcntl(fd, F_SETFL, O_NONBLOCK);
    
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = inet_addr("0.0.0.0");
    serverAddr.sin_port = htons(port);
    
    auto status = bind(fd, (sockaddr*)&serverAddr, addrLen);
    if (status < 0) std::exit(2);
}

int Server::recvMessage() {
    static constexpr std::size_t bufferLen = 2048;
    
    std::array<char, bufferLen> buffer;
    Addr addr;  //client
    socklen_t clientLen = addrLen;
    
    auto status =
        recvfrom(fd, buffer.data(), bufferLen, 0, (sockaddr*)&addr, &clientLen);
    if (status < 0) return status;
    
    Data::ClientMessage msg;
    yas::mem_istream is(buffer.data(), status);
    yas::binary_iarchive<yas::mem_istream> ia(is);
    ia.serialize(msg);
    
    world->recvMessage(addr, msg);
    return status;
}

void Server::recvMessages() {
    while(recvMessage() >= 0);
}

void Server::addMessage(const Addr& addr, Data::ServerMessage& msg) {
    msg.id = acc++;
    outQueue.push_back(std::make_pair(addr, msg));
}

int Server::sendMessage(const Addr& addr, const Data::ServerMessage& msg) {
    static constexpr std::size_t bufferLen = 2048;
    
    std::array<char, bufferLen> buffer;
    yas::mem_ostream os(buffer.data(), bufferLen);
    yas::binary_oarchive<yas::mem_ostream> oa(os);
    oa.serialize(msg);
    
    auto status =
        sendto(fd, buffer.data(), os.size(), 0, (sockaddr*)&addr, addrLen);
    
    return status;
}

void Server::sendMessages() {
    for (auto& pair : outQueue)
        sendMessage(pair.first, pair.second);
    outQueue.clear();
}
