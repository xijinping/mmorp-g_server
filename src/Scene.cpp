#include "Scene.h"
#include "Chunk.h"

Scene::Scene(int id, int w, int h)
: width(w)
, height(h)
, rect({0, 0, w*Data::Chunk::size, h*Data::Chunk::size})
{
    this->id = id;
    auto size = w*h;
    
    chunks.reserve(size);
    for (auto i = 0; i < size; ++i)
        chunks.emplace_back(i, Point{i%w, i/w});
}

Chunk* Scene::getChunk(const Point& point) {
    return getChunk(point.x, point.y);
}

Chunk* Scene::getChunk(int x, int y) {
    return &chunks[x+y*width];
}

Data::Scene Scene::toData() {
    Data::Scene ret;
    ret.id = id;
    ret.width = width;
    ret.height = height;
    ret.chunks.reserve(chunks.size());
    for(auto& chunk : chunks)
        ret.chunks.push_back(chunk.toData());
    return ret;
}
