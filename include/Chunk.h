#pragma once

#include "Includes.h"

struct Character;

class Chunk {
    using TileRow = std::array<uint32_t, Data::Chunk::size>;
    using TileMap = std::array<TileRow, Data::Chunk::size>;
    
    int id;
    Point pos;
    Point realPos;
    TileMap tileMap;
    
public:
    Chunk(int id, const Point& pos);
    
    std::unordered_set<Character*> characters;
    uint32_t& getFlags(const Point& point);
    
    Data::Chunk toData();
};
