#pragma once

#include "Includes.h"
#include "Chunk.h"

class Scene {
    int id;
    int width;
    int height;
    std::vector<Chunk> chunks;
    
    friend class World;
    friend struct Character;
    
public:
    Rect rect;
    
    Scene(int id, int w, int h);
    
    Chunk* getChunk(const Point& point);
    Chunk* getChunk(int x, int y);
    
    Data::Scene toData();
};
