#pragma once

#include "Includes.h"

class Scene;
class Chunk;

struct Character : public Data::Character {
    Character(int id, Scene* scene, Point spawn, Addr owner);
    ~Character();
    
    void move(const Point& point);
    std::vector<Chunk*> getNearbyChunks();
    
    void update(float delta);
    void sendChunks();
    void giveControl();
    
    Scene* scene;
    Chunk* chunk;
    Addr client;
    bool dirty;
};
