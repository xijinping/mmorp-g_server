#pragma once

#include "Includes.h"
#include "Scene.h"

class Server;
struct Character;

class World {
    int acc;
    Scene scene;
    
    std::unordered_map<Addr, Character*> clientCharacters;
    
    Character* addCharacter(const Addr& addr);
    void remCharacter(const Addr& addr);
    
    void moveCharacter(const Addr& addr, const Data::ClientMessage& msg);
    void attackCharacter(const Addr& addr, const Data::ClientMessage& msg);
    
    void addClient(const Addr& addr);
    void remClient(const Addr& addr);
    
public:
    World();
    
    void recvMessage(const Addr& addr, const Data::ClientMessage& msg);
    
    void update(float delta);
};

extern World* world;
