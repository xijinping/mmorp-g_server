#pragma once

#include "Types.h"
#include <cmath>

inline Point sub(const Point& r, const Point& l) {
    return Point{r.x-l.x, r.y-l.y};
}

inline int length(const Point& point) {
    return std::abs(point.x) + std::abs(point.y);
}

inline bool inRect(const Point& point, const Rect& rect) {
    return (point.x >= rect.x && point.x < rect.x + rect.w)
        && (point.y >= rect.y && point.y < rect.y + rect.h);
}
