#pragma once

#include "Includes.h"

class Server {
    int acc;
    
    int fd;
    Addr serverAddr;
    
    std::vector<std::pair<Addr, Data::ServerMessage>> outQueue;
    
    int recvMessage();
    int sendMessage(const Addr& addr, const Data::ServerMessage& msg);
    
public:
    Server(short port);
    
    void addMessage(const Addr& addr, Data::ServerMessage& msg);
    
    void recvMessages();
    void sendMessages();
};

extern Server* server;
